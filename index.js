/**
 * mark > 90 = A+
 * mark > 80 = A
 * mark > 70 = B+
 * mark > 60 = B
 * mark > 50 = C
 * mark < 50 = failed
 */

let mark = 30

if (mark >= 90) {
    console.log("A+")    
}
else if (mark >= 80) {
    console.log("A")
}
else if (mark >= 70) {
    console.log("B+")
}
else if (mark >= 60) {
    console.log("B")
}
else if (mark >= 50) {
    console.log("C+")
}
else{
    console.log("FAILED,Try again...!")
}

